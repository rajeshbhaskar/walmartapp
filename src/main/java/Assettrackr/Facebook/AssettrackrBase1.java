package Assettrackr.Facebook;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class AssettrackrBase1 
{
	public static AndroidDriver<AndroidElement> driver=null;
	public static AndroidDriver<AndroidElement> capabilities() throws MalformedURLException
	{
		if(driver == null)
		{
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
		cap.setCapability("appActivity", "com.walmart.ewolve.memberapp.MainActivity");
		cap.setCapability("appPackage", "in.bestprice.android"); 
		driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),cap);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
		}
		else
		{
			return driver;
		}
	}

}

