package Assettrackr.Facebook;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import static java.time.Duration.ofSeconds;
import io.appium.java_client.touch.*;

import org.openqa.selenium.WebElement;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.offset.PointOption;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import static io.appium.java_client.touch.TapOptions.tapOptions;
import org.openqa.selenium.WebElement;
import static io.appium.java_client.touch.offset.ElementOption.element;
public class LoginPenelopTest extends AssettrackrBase1
{
	TouchAction t,action;
	@Test
	public void walmart_Test1() throws MalformedURLException, InterruptedException
	{
		System.out.println("Inside loginTest1");
		AndroidDriver<AndroidElement> driver= capabilities();
		Thread.sleep(5000);
		for(int i=0;i<4;i++)
		{
		driver.findElementByXPath("//android.widget.TextView[@text='Next']").click();
		}
		driver.findElementByXPath("//android.widget.TextView[@text='Get Started']").click();
		Thread.sleep(5000);
		driver.findElementByXPath("//android.widget.EditText[@text='Search for Products and more']").click();
		driver.findElementByXPath("//android.widget.EditText[@text='Search for Products and more']").sendKeys("Chocolate");
		Thread.sleep(5000);
		driver.findElementsByXPath("//android.widget.TextView[@text='Search']").get(1).click();
		String containedText="Complan Pista Badam Refill Pack, 500 g";
		Thread.sleep(5000);
		driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + containedText + "\").instance(0))"));
	    t =new TouchAction(driver);
		WebElement expandList=	driver.findElementByXPath("//android.widget.TextView[@text='Complan Pista Badam Refill Pack, 500 g']");
		t.tap(tapOptions().withElement(element(expandList))).perform();
		driver.findElementByXPath("//android.widget.TextView[@text='Login to Buy']").click();
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();	
	}
	@Test
	public void walmart_Test2() throws MalformedURLException, InterruptedException
	{
		AndroidDriver<AndroidElement> driver1= capabilities();
		System.out.println("Inside Test2");
		driver1.findElementByXPath("//android.widget.Button[@content-desc='Categories, tab, 2 of 5']").click();
		driver1.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + "Health & OTC" + "\").instance(0))"));
		driver1.findElementByXPath("//android.widget.TextView[@text='Health & OTC']").click();
		driver1.findElementByXPath("//android.widget.TextView[@text='Health Equipments']").click();
		driver1.findElementByXPath("//android.widget.TextView[@text='Login to Buy']").click();
		driver1.navigate().back();
		driver1.navigate().back();
	}
	@Test
	public void walmart_Test3() throws MalformedURLException, InterruptedException
	{
		AndroidDriver<AndroidElement> driver1= capabilities();
		System.out.println("Inside Test3");
		driver1.findElementByXPath("//android.widget.Button[@content-desc='Login Now!, tab, 3 of 5']").click();
		driver1.findElementByXPath("//android.widget.EditText[@text='Membership ID/ Phone No.']").sendKeys("12345678");
		
		MobileElement element = (MobileElement) driver1.findElementByXPath("//android.widget.EditText[@text='12345678']");
		element.clear();
		driver1.findElementByXPath("//android.widget.TextView[@text='Apply for Membership']").click();
		Thread.sleep(9000);
		driver1.findElementByXPath("//android.view.View[@text='Select Store']").click();
		Thread.sleep(5000);
		int list =1;
		Dimension size = driver.manage().window().getSize();
		   int height = size.getHeight();
		   int width = size.getWidth();
		   int x = width/3;
		   int starty = (int)(height*0.70);
		   int endy = (int)(height*0.50);
		 if (list != 0) 
		 { 
	        new TouchAction(driver).press(PointOption.point(x, starty)).waitAction(WaitOptions.waitOptions(ofSeconds(2))).moveTo(PointOption.point(x, endy)).release().perform();
	        new TouchAction(driver).press(PointOption.point(x, starty)).waitAction(WaitOptions.waitOptions(ofSeconds(2))).moveTo(PointOption.point(x, endy)).release().perform();
	        driver1.findElementByXPath("//android.view.View[@text='JAMMU']").click();
	        driver1.findElementByXPath("//android.widget.Button[@text='Go']").click();
	        Thread.sleep(10000);
	        //driver1.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" +"Store*"+ "\").instance(0))"));
		 /*  TouchAction touchAction = new TouchAction(driver);
		  // touchAction.moveTo(moveToOptions)
		  // touchAction.press(pressOptions(bottomElement)).
		   touchAction.press(point(x,y)).moveTo(topElement).release();
		   touchAction.perform();*/
		   
		 }
		//driver1.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\"" + "WARANGAL" + "\").instance(0))"));
		int count = driver1.findElementsByXPath("//android.widget.EditText").size();
		System.out.println("Total Size is "+ count);
		starty = (int)(height*0.70);
		endy = (int)(height*0.50);
		new TouchAction(driver).press(PointOption.point(x, starty)).waitAction(WaitOptions.waitOptions(ofSeconds(2))).moveTo(PointOption.point(x, endy)).release().perform();
		System.out.println("Before loop "+driver.getPageSource());
		for(int i=0;i<=count ;i++)
		{
			if(i==0)
				driver1.findElementsByXPath("//android.widget.EditText").get(i).sendKeys("Raj");
			if(i==1)
				driver1.findElementsByXPath("//android.widget.EditText").get(i).sendKeys("kumar");
			if(i==2)
				driver1.findElementsByXPath("//android.widget.EditText").get(i).sendKeys("Bhaskar");
			if(i==3)
			{
				driver1.findElementsByXPath("//android.widget.EditText").get(i).sendKeys("8970803156");
			}
			if(i==4)
				driver1.findElementsByXPath("//android.widget.EditText").get(i).sendKeys("rajesh@gmail.com");
		}
		System.out.println("Before "+driver.getPageSource());
		starty = (int)(height*0.80);
		endy = (int)(height*0.40);
		action = new TouchAction(driver1);
		Dimension size1 = driver.manage().window().getSize();
		   int height1 = size.getHeight();
		   int width1 = size.getWidth();
		   int x1 = width/2;
		   int y1 = (int)(height1*0.75);
		new TouchAction(driver).press(PointOption.point(x, starty)).waitAction(WaitOptions.waitOptions(ofSeconds(2))).moveTo(PointOption.point(x, endy)).release().perform();
		action.tap(PointOption.point(x1, y1)).release().perform();
		Thread.sleep(4000);
		starty = (int)(height*0.70);
		endy = (int)(height*0.50);
		 new TouchAction(driver).press(PointOption.point(x, starty)).waitAction(WaitOptions.waitOptions(ofSeconds(2))).moveTo(PointOption.point(x, endy)).release().perform();
	     new TouchAction(driver).press(PointOption.point(x, starty)).waitAction(WaitOptions.waitOptions(ofSeconds(2))).moveTo(PointOption.point(x, endy)).release().perform();
	     y1 = (int)(height1*0.70);
	     action.tap(PointOption.point(x1, y1)).release().perform();
	     //driver1.findElementByXPath("//android.view.View[@text='JAMMU']").click();
		//driver1.findElementByXPath("//android.view.View[@text='.']").click();
		
		
		System.out.println(driver.getPageSource());
		
	}
}
		//driver.findElementByXPath("//android.widget.EditText[@text='Email / Username']").sendKeys("john.doe@transics.com");
		//driver.findElementByXPath("//android.widget.EditText[@text='Password']").sendKeys("Test1234!");
		//driver.findElementByXPath("//android.widget.TextView[@text='SIGN IN']").click();
		//driver.findElementByXPath("//android.widget.Button[@text='ALLOW']").click();
		//System.out.println("Passed");
		
		// Secont Test Case
		//driver.findElementByXPath("//android.widget.TextView[@text='MESSAGING']").click();
		//driver.findElementByXPath("//android.widget.TextView[@text='+']").click();
		//driver.findElementByXPath("//android.widget.EditText[@text='Type your message content']").sendKeys("Hi Abhinav");
		//driver.findElementByXPath("//android.widget.TextView[@text='SELECT ATTACHMENT']").click();
		//driver.findElementByXPath("//android.widget.TextView[@text='TAKE A PICTURE']").click(); 
		//driver.findElementByXPath("//android.widget.TextView[@text='Take Photo…']").click();
		//driver.findElementByXPath("//android.widget.Button[@text='ALLOW']").click();
		//driver.findElementByXPath("//android.widget.Button[@text='ALLOW']").click();
		//Thread.sleep(20000);
		//driver.findElement(By.id("com.motorola.ts.camera:id/shutter_button")).click();
		//com.motorola.ts.camera:id/preview_btn_done
		//Thread.sleep(20000);
		//driver.findElement(By.id("com.motorola.ts.camera:id/preview_btn_done")).click();
		//driver.findElementByXPath("//android.widget.Button[@text='SEND MESSAGE']").click();

//driver.findElementsByClassName("android.widget.MultiAutoCompleteTextView").get(1).sendKeys("Rajesh");

		//driver.startActivity(new Activity("com.facebook.lite", ".MainActivity"));
		//driver.findElementByXPath("//android.widget.EditText[@text='Email / Username']").sendKeys("john.doe@transics.com");
		//driver.findElementByXPath("//android.widget.EditText[@text='Password']").sendKeys("Test1234!");
		//driver.findElementByXPath("//android.widget.TextView[@text='SIGN IN']").click();
		//driver.findElementByXPath("//android.widget.Button[@text='ALLOW']").click();
		//System.out.println("Passed");
		
		// Secont Test Case
		//driver.findElementByXPath("//android.widget.TextView[@text='MESSAGING']").click();
		//driver.findElementByXPath("//android.widget.TextView[@text='+']").click();
		//driver.findElementByXPath("//android.widget.EditText[@text='Type your message content']").sendKeys("Hi Abhinav");
		//driver.findElementsByClassName("android.widget.Button").get(2).click();
		//String text = driver.findElement(By.id("android:id/alertTitle")).getText();
		//System.out.println("String is"+text);
		//String actual = "Message Sent!";
		//driver.findElementByClassName("android.widget.Button").click();
		//System.out.println("Hello new commit");
		
		//Bubble Message	